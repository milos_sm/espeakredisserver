#include <cpp_redis/cpp_redis>
#include <tacopie/tacopie>
#include <espeak-ng/speak_lib.h>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <signal.h>
#include <getopt.h>
#include "document.h"
#include "encodings.h"
#include "stringbuffer.h"
#include "prettywriter.h"
#include <codecvt>
#include <locale>

using namespace rapidjson;
typedef GenericDocument<UTF16<> > WDocument;
typedef GenericValue<UTF16<> > WValue;
typedef GenericStringBuffer<UTF16<> > WStringBuffer;

std::condition_variable should_exit;
bool isESpeakInitialized = false;
cpp_redis::future_client* redisClient;
bool redisConnected = false;
std::string serverIP = "127.0.0.1";
int port = 6379;
std::string password = "";

std::string w_to_s(const std::wstring & wstr)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> strconverter;
	return strconverter.to_bytes(wstr);
}

std::string w_to_s(const wchar_t* wstr)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> strconverter;
    return strconverter.to_bytes(wstr);
}

std::wstring s_to_w(const std::string & str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
	return converter.from_bytes(str);
}

std::wstring s_to_w(const char* str)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.from_bytes(str);
}

void sigint_handler(int) 
{
  should_exit.notify_all();
}

bool initializeESpeak()
{
    int result = 0;
    if (!isESpeakInitialized)
    {
        result = espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
    }
    
    if (result == EE_INTERNAL_ERROR)
    {
        std::cout << "initializeESpeak: eSpeak initialization failed." << std::endl;
        return false;
    }

    std::cout <<"initializeESpeak: eSpeak initialization complete." << std::endl;

    return true;
}

bool getPhonemesFromESpeak(std::string language, std::vector<std::vector<std::wstring>> normalizedInput, std::vector<std::vector<std::wstring>>& phonemes)
{
    if (!isESpeakInitialized)
    {
        std::cout << "getPhonemesFromESpeak:  eSpeak not initialized!" << std::endl;
        return false;
    }

    try
    {

        espeak_ERROR err = espeak_SetVoiceByName(language.c_str());
    
        if (err != EE_OK)
        {
            std::cout << "initializeESpeak failed to initialize espeak module for language: " << language << ". Error: " << std::to_string(err) << std::endl;
            return false;
        }
    
        const char* output = nullptr;
    
        std::vector<std::wstring> sentencePhonemes;

        // Do french separately due to espeak bug with wchar
        if(language.compare("fr")==0)
        {
            for (unsigned int i = 0; i < normalizedInput.size(); i++)
            {
                sentencePhonemes.clear();
                for (unsigned int j = 0; j < normalizedInput[i].size(); j++)
                {
                    std::string current = w_to_s(normalizedInput[i][j]);
                    const char* stringPtr = current.c_str();
                    const char** ptrToString = &stringPtr;
                    output = espeak_TextToPhonemes((const void**)ptrToString, espeakCHARS_AUTO, espeakPHONEMES_IPA);
                    
                    if (output == NULL)
                    {
                        std::cout << "getPhonemesFromESpeak failed to retreive phonemes.";
                        return false;
                    }
    
                    sentencePhonemes.push_back(s_to_w(output));
                }
        
                phonemes.push_back(sentencePhonemes);
            }
        }
        else
        {
            for (unsigned int i = 0; i < normalizedInput.size(); i++)
            {
                sentencePhonemes.clear();
                for (unsigned int j = 0; j < normalizedInput[i].size(); j++)
                {
                    std::wstring current = normalizedInput[i][j];
                    const wchar_t* wstringPtr = current.c_str();
                    const wchar_t** ptrToString = &wstringPtr;
                    output = espeak_TextToPhonemes((const void**)ptrToString, espeakCHARS_WCHAR, espeakPHONEMES_IPA);
                    
                    if (output == NULL)
                    {
                        std::cout << "getPhonemesFromESpeak failed to retreive phonemes.";
                        return false;
                    }
    
                    sentencePhonemes.push_back(s_to_w(output));
                }
        
                phonemes.push_back(sentencePhonemes);
            }
        }

    }
    catch(const std::exception& e)
    {
        std::cout << "getPhonemesFromESpeak - ERROR: " << e.what() << std::endl;
        return false;
    }
    return true;
}

bool parseInputJsonToStrings(const std::string inputJSON, std::string& language, int& requestID, std::vector<std::vector<std::wstring>>& normalizedInput)
{
    WDocument inputDocument;

    inputDocument.Parse(s_to_w(inputJSON).c_str());

    if (!inputDocument.IsObject())
    {
        std::cout << "parseInputJsonToStrings:  Invalid JSON!" << std::endl;
        return false;
    }

    if (!inputDocument.HasMember(L"requestID"))
    {
        std::cout << "parseInputJsonToStrings: JSON does not have requestID node!" << std::endl;
        return false;
    }

    const WValue& jsonRequestID = inputDocument[L"requestID"];

    if(jsonRequestID.IsInt())
    {
        requestID = jsonRequestID.GetInt();
    }
    else
    {
        std::cout << "parseInputJsonToStrings: JSON requestID node is invalid." << std::endl;
        return false;
    }

    if (!inputDocument.HasMember(L"language"))
    {
        std::cout << "parseInputJsonToStrings: JSON does not have language node!" << std::endl;
        return false;
    }

    const WValue& jsonLanguage = inputDocument[L"language"];

    language = w_to_s(jsonLanguage.GetString());

    if (!inputDocument.HasMember(L"normalizedInput"))
    {
        std::cout << "parseInputJsonToStrings: JSON does not have normalizedInput node!" << std::endl;
        return false;
    }

    const WValue& jsonNormalizedInput = inputDocument[L"normalizedInput"];

    if (!jsonNormalizedInput.IsArray())
    {
        std::cout << "parseInputJsonToStrings: normalizedInput array is corrupt!" << std::endl;
        return false;
    }

    for (WValue::ConstValueIterator itr = jsonNormalizedInput.Begin(); itr != jsonNormalizedInput.End(); ++itr)
    {
        std::string sentenceAccumulation = "";
        const WValue& jsonSentence = *itr;
        std::vector<std::wstring> sentence;

        if (!jsonSentence.IsArray())
        {
            std::cout << "parseInputJsonToStrings: jsonSentence array is corrupt!" << std::endl;
            return false;
        }

        for (WValue::ConstValueIterator itr1 = jsonSentence.Begin(); itr1 != jsonSentence.End(); ++itr1)
        {
            const WValue& jsonSentencePart = *itr1;
            sentence.push_back(jsonSentencePart.GetString());
        }

        normalizedInput.push_back(sentence);
    }

    return true;
}

bool phonemesToJson(int requestID, std::vector<std::vector<std::wstring>> phonemes, std::wstring& outputJSON)
{
    WDocument outputDocument;
    WValue toJsonValue;
    
    // Write input sentences to JSON string.
    outputDocument.SetObject();
    rapidjson::Document::AllocatorType& allocator = outputDocument.GetAllocator();

    outputDocument.AddMember(L"requestID", requestID, allocator);
    WValue jsonPhonemesOutput(kArrayType);

    for (unsigned int i = 0; i < phonemes.size(); i++)
    {
        WValue jsonPhonemesPartsVector(kArrayType);

        for (unsigned int j = 0; j < phonemes[i].size(); j++)
        {
            std::wstring toJson = phonemes[i][j];
            toJsonValue.SetString(toJson.c_str(), toJson.length(), allocator);
            jsonPhonemesPartsVector.PushBack(toJsonValue, allocator);
        }

        jsonPhonemesOutput.PushBack(jsonPhonemesPartsVector, allocator);
    }

    outputDocument.AddMember(L"phonemes", jsonPhonemesOutput, allocator);
    WStringBuffer strbuf;
    rapidjson::PrettyWriter<WStringBuffer, UTF16<>, UTF16<>> writer(strbuf);
    outputDocument.Accept(writer);
    outputJSON = strbuf.GetString();

    std::cout << "outputJSON: " << w_to_s(outputJSON) << std::endl;

    return true;
}

void redisReconnectCallback(cpp_redis::redis_client& redis_client) 
{
	try
	{		
		redis_client.connect(serverIP, port, redisReconnectCallback);
        if(!password.empty())
        {
		    redis_client.auth(password);
        }
		redisConnected = true;
	}
	catch (const std::exception& ex)
	{
	    std::cout << "no connection to Redis " << serverIP << ":" << std::to_string(port) << ". " << ex.what() << std::endl;
		redisConnected = false;
	}
	catch (...)
	{
		std::cout << "unspecified exception - no connection to Redis " << serverIP << ":" << std::to_string(port) << std::endl;
		redisConnected = false;
	}
}

void processSentences(const std::string& msg)
{
    std::vector<std::vector<std::wstring>> input;
    std::vector<std::vector<std::wstring>> phonemes;
    std::string language = "";
    int requestID=0;
    std::wstring outputJSON;
    std::vector<std::string> values;

    if(parseInputJsonToStrings(msg, language, requestID, input))
    {
        if(getPhonemesFromESpeak(language, input, phonemes))
        {
            if(!phonemesToJson(requestID, phonemes, outputJSON))
            {
                outputJSON = L"";
                std::cout << "processSentences: phonemesToJson failed!" << std::endl;
            }
        }
    }
    else
    {
        outputJSON = L"";
        std::cout << "processSentences: parseInputJsonToStrings failed!" << std::endl;
    }

    if(redisConnected)
    {
        if (redisClient->is_connected())
	    {
            values.clear();
	    	values.push_back(w_to_s(outputJSON).c_str());
            std::string key = "ESpeakPhonemes_" + std::to_string(requestID);
	    	auto set = redisClient->rpush(key, values);
	    	redisClient->sync_commit();
	    }
        else
        {
            std::cout << "processSentences: Error - Redis client is not connected!" << std::endl;
        }
    }
    else
    {
        std::cout << "processSentences: Can't push message to redis. Redis is not connected." << std::endl;
    }
}

int main(int argc, char *argv[]) 
{
    int c;
    int digit_optind = 0;
  
    //! Enable logging
    cpp_redis::active_logger = std::unique_ptr<cpp_redis::logger>(new cpp_redis::logger);
  
    isESpeakInitialized = initializeESpeak();
  
    if(!isESpeakInitialized)
    {
        std::cout << "ESpeak NOT initialized! Exit espeak redis server. " << std::endl;
        exit(1);
    }

    while (1) 
    {
        int option_index = 0;
        static struct option long_options[] = {
                {"host",      required_argument, 0,  0    },
                {"port",      required_argument, 0,  6379 },
                {"password",  required_argument, 0,  0    },
                {0,                           0, 0,  0    }
        };

        c = getopt_long(argc, argv, "h:p:s:", long_options, &option_index);
        if (c == -1)
        {
            break;
        }

        switch (c) 
        {
        case 0:
            printf("option %s", long_options[option_index].name);
            if (optarg)
                printf(" with arg %s", optarg);
                printf("\n");
                break;
        case 'h':
            serverIP = optarg;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 's':
            password = optarg;
            break;
        case '?':
            break;
        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc) 
    {
        printf("non-option ARGV-elements: ");
        while (optind < argc)
        {
            printf("%s ", argv[optind++]);
        }
        printf("\n");
    }

    if ((port < 0) && (port > 65535))
    {
        std::cout << "ERROR: Port is out of range 0-65535. Using default 6379." << std::endl;
        port = 6379;
    }

    redisClient = new cpp_redis::future_client();
    try
    {
        std::cout << "Connecting to address: " << serverIP << std::endl;
        std::cout << "Connecting to port: " << port << std::endl;
        redisClient->connect(serverIP, port, redisReconnectCallback);
        redisClient->auth(password);
    }
    catch(const std::exception& e)
    {
        redisConnected = false;
        std::cout << "Redis connection failed." << e.what() << '\n';
    }

    redisConnected = true;
    std::vector<std::string> queue;
    std::string jsonSentences;
    queue.push_back("ESpeakSentences");

    try
    {
        do
        {
            if (redisClient->is_connected())
	        {
                try
                {
                    auto get = redisClient->blpop(queue, 30);
	    	        redisClient->sync_commit();
	    	        auto getResult = get.get();
	    	        if (getResult.is_array())
	    	        {
	    	        	jsonSentences = getResult.as_array().at(1).as_string();
                        processSentences(jsonSentences);
	    	        }
                    else
                    {
                        std::time_t t = std::time(0);
                        std::cout << std::ctime(&t) << "Result is not array. Ignoring it." << std::endl;
                    }
                }
                catch(const std::exception& e)
                {
                    std::cout << "Exception during redis read. " << e.what() << '\n' << std::endl;
                }
	        }
            else
            {
                std::cout << "Redis client not connected. Attempt to reconnect." << std::endl;
                redisClient->connect(serverIP, port, redisReconnectCallback);
                redisClient->auth(password);
                std::cout << "Sleep 2 seconds before another reconnection attempt." << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            }
        } while (1);
    }
    catch(const std::exception& e)
    {
        std::cout << e.what() << '\n';
    }

  delete redisClient;

  return 0;
}