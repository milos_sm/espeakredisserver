espeakredisserver

Implementation of simple espeak server that communicates with outside world via redis. 
It is intended to be as simple as possible so that it be very light when packed to Docker. 

Reason for development was single threaded nature od espeak. It was not possible to use espeak 
in multithreaded way so the solution was to create simple server approchable via redis. This way 
each thread from SRS could have one instance of the espeak redis server on it's disposale. 

USAGE:

espeakredisserver -hSERVER_HOST -pPORT -sPASSWORD

Where SERVER_HOST is address of the redis server, PORT is port of the redis server and PASSWORD is password to the redis server. 
 
EXAMPLE:

espeakredisserver -h3.249.32.248 -p6379 -sazmwR3vHdBWSKJRb

INPUT AND OUTPUT

To send a request user needs to send redis request with key ESpeakSentences with following JSON contents:

{
    "requestID" : 400,
    "language" : "en-us",
    "normalizedInput" : [["Hello world."],["Lovely weather today."]]
}

Here is the explanation of the fields:
    - request ID - Request number. It is used in creation of the response key. Response key will be ESpeakSentences_+requestID . In our example it would be ESpeakSentences_400 .
    - language - sentence language ID as defined in espeak github repo here https://github.com/espeak-ng/espeak-ng/blob/master/docs/languages.md .
    - normalized input - Array of sentences to process.

To receive requests, user needs to look for key ESpeakSentences_+requestID where requestID is sent inside redis request. See above. Received JSON response will look like this:

{
  "requestID": 400,
  "phonemes": [
    [
      " həlˈoʊ wˈɜːld"
    ],
    [
      " lˈʌvli wˈɛðɚ tədˈeɪ"
    ]
  ]
}

Here is the explanation of the fields:
    - request ID - Request number. It is used in creation of the response key. Response key will be ESpeakSentences_+requestID . In our example it would be ESpeakSentences_400 .
    - phonemes - Array of sentances in phoneme representation.



